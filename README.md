# SciMap

**IMPORTANT:** This interface was designed for a tablet/medium-size screen.
If running on desktop, use Chrome developer tools and set resolution to
750 x 450.

How to run:
1. Install Node.js
2. Run command 'npm run dev' in api folder
3. Run command 'nom run serve' in client folder
4. View website on localhost:8080

Authors:
- Stefan Stegić (SW61-2017)
- Luka Nikolić (SW64-2017)

Uses express in node.js for back-end and vue.js for front-end.
Database is SQLite.