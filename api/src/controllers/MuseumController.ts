import MuseumService from "../services/MuseumService";

class MuseumController {
    public async getAll(req: any, res: any) {
        try {
            const museums = await MuseumService.getAll();
            return res.send(museums);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async getOne(req: any, res: any) {
        try {
            const museums = await MuseumService.getOne(req.params["id"]);
            return res.send(museums);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async create(req: any, res: any) {
        try {
            const museums = await MuseumService.create(req.body);
            return res.send(museums);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async delete(req: any, res: any) {
        try {
            await MuseumService.delete(req.params["id"]);
            return res.send();
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async update(req: any, res: any) {
        try {
            const museums = await MuseumService.update(req.body);
            return res.send(museums);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async uploadImg(req: any, res: any) {
        try {
            if (!req.files || Object.keys(req.files).length === 0) {
                return res.status(400).send('No files were uploaded.');
            }

            const path = await MuseumService.setImage(req.params["id"], req.files.image);
            return res.send(path);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async updateLink(req: any, res: any) {
        try {
            await MuseumService.updateLink(req.body.museumId, req.body.scientistId);
            return res.send();
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }
}

export default new MuseumController;