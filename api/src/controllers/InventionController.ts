import InventionService from "../services/InventionService";

class InventionController {
    public async getAll(req: any, res: any) {
        try {
            const inventions = await InventionService.getAll();
            return res.send(inventions);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async getOne(req: any, res: any) {
        try {
            const invention = await InventionService.getOne(req.params["id"]);
            return res.send(invention);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async create(req: any, res: any) {
        try {
            const invention = await InventionService.create(req.body);
            return res.send(invention);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async delete(req: any, res: any) {
        try {
            await InventionService.delete(req.params["id"]);
            return res.send();
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async update(req: any, res: any) {
        try {
            const invention = await InventionService.update(req.body);
            return res.send(invention);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }
}

export default new InventionController;