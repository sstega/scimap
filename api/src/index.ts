import db from "./models/database"
import config from "./config"
import express, { Application } from "express";
import initModel from "./models/initModel";
import initModelOneEach from "./models/initModelOneEach";
import scientistRoutes from "./routes/ScientistRoutes";
import inventionRoutes from "./routes/InventionRoutes";
import museumRoutes from "./routes/MuseumRoutes";
import cors from "cors";
import bodyParser from "body-parser";

(async () => {
    try {
        await db
            .authenticate()
            .then(() => console.log("Connected to SQLite database"))
            .catch((e) => {
                console.error("There was an error while trying to connect to the database: " + e);
            });
        // Uncomment next line to sync database tables with model
        // WARNING: Wipes all data!
        await db.sync({ force: true });
        await initModel();
        //await initModelOneEach();
    } catch (e) {
        console.error(e);
    }
})();

const app: Application = express();

// Middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
app.use("/images", express.static('images'));
app.use("/scientists", scientistRoutes);
app.use("/inventions", inventionRoutes);
app.use("/museums", museumRoutes);


// Finally, listen to requests
app.listen(config.port, () => {
    console.log(`Server listening on port ${config.port}`);
});