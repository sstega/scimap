import ScientistService from "../services/ScientistService";
import InventionService from "../services/InventionService";
import MuseumService from "../services/MuseumService";

export default async function initModel() {
    console.log("Running initModel 'one each' script");
    
    const sci: any = await ScientistService.create({
        label: "TSL",
        firstName: "Nikola",
        lastName: "Tesla",
        yearOfBirth: 1856,
        yearOfDeath: 1943,
        placeOfBirth: "Smiljan",
        POBLat: 49,
        POBLng: -1,
        nationality: "Serbian",
        science: "Engineering",
        nobelPrize: false
    });

    await InventionService.create({
        scientistId: sci.id,
        name: "Alternating current"
    });

    await InventionService.create({
        scientistId: sci.id,
        name: "Tesla coil"
    });

    const mus: any = await MuseumService.create({
        name: "Muzej Prirodnih Nauka",
        location: "Beograd",
        openingDate: "2020-06-23",
        showcasedScientistId: null,
        description: "Muzej koji otvara oci",
        showcase: false,
        showcaseStartDate: null,
        showcaseEndDate: null,
        lat: 40,
        lng: -20,
    });
}