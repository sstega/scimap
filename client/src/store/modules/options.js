export default {
    namespaced: true,

    state: {
        mode: "all",
        nextMode: {
            "all": { icon: "mdi-account", color: "#ff8080" },
            "scientists": { icon: "mdi-bank", color: "yellow lighten-1" },
            "museums": { icon: "mdi-close", color: "white" },
        },
    },

    mutations: {
        setModeScientists(state){
            state.mode = "scientists";
        },

        setModeMusems(state){
            state.mode = "museums";
        },

        toggleMode(state) {
            if (state.mode === "all") {
                state.mode = "scientists";
            } else if (state.mode === "scientists") {
                state.mode = "museums";
            } else {
                state.mode = "all";
            }
        }
    },

    actions: {
       
    },

    getters: {
        getModeIcon: (state) => state.nextMode[state.mode].icon,
        getModeColor: (state) => state.nextMode[state.mode].color,
        getModeName: (state) => state.mode
    }
}
