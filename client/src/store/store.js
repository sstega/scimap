import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";

import scientists from "./modules/scientists";
import museums from "./modules/museums";
import options from "./modules/options";
import sidebar from "./modules/sidebar";

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
  key: "my-app",
  storage: window.localStorage,
});

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  modules: {
    scientists,
    museums,
    options,
    sidebar
  },
});
